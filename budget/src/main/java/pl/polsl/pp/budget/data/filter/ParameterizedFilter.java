package pl.polsl.pp.budget.data.filter;

public interface ParameterizedFilter<PARAMETERS> {

	<RESULT> RESULT filter(PARAMETERS parameters);
}
