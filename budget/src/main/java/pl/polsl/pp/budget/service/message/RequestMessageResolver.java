package pl.polsl.pp.budget.service.message;

/**
 * Created by Piotrek on 2015-02-08.
 */
public interface RequestMessageResolver {

    String resolve(String code);
}
