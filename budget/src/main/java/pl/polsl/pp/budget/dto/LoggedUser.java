package pl.polsl.pp.budget.dto;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.polsl.pp.budget.domain.User;

public class LoggedUser extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = -1270620923763128517L;
	private User user;

	public LoggedUser(User user) {
		super(user.getLogin(), user.getPassword(), createGrantedAuthorities(user));
		this.user = user;
	}

	private static Collection<GrantedAuthority> createGrantedAuthorities(User user) {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		return grantedAuthorities;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
