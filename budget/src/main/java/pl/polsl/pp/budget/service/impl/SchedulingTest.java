package pl.polsl.pp.budget.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.quartz.CronExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.MoneyFlow;
import pl.polsl.pp.budget.domain.PendingMoneyFlow;

@Service
public class SchedulingTest {

	@Resource
	private ParameterizedFilter<Date> pendingMoneyFlowsAfterDate;

	@Resource
	private UniversalDao dao;

	private Date executionTime;

	private List<MoneyFlow> moneyFlows = new ArrayList<>();

	@Scheduled(cron = "*/5 * * * * *")
	@Transactional
	public void doSomething1() {
		executionTime = new Date();
		moneyFlows = new ArrayList<>();
		System.out.println(executionTime);
		List<PendingMoneyFlow> pendingMoneyFlows = pendingMoneyFlowsAfterDate.filter(executionTime);
		pendingMoneyFlows.stream().forEach(this::propagate);
		pendingMoneyFlows.stream().forEach(dao::save);
		moneyFlows.stream().forEach(dao::save);
	}

	private void propagate(PendingMoneyFlow pendingMoneyFlow) {
		System.out.println(pendingMoneyFlow.getAmount());
		try {
			CronExpression cronExpression = new CronExpression(pendingMoneyFlow.getCronExpression());

			Date recalculatedNextExecution = pendingMoneyFlow.getDate();
			do {
				recalculatedNextExecution = cronExpression.getNextValidTimeAfter(recalculatedNextExecution);
				moneyFlows.add(createMoneyFlow(pendingMoneyFlow, recalculatedNextExecution));
			} while (recalculatedNextExecution.before(executionTime));
			pendingMoneyFlow.setDate(recalculatedNextExecution);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private MoneyFlow createMoneyFlow(PendingMoneyFlow pendingMoneyFlow, Date recalculatedNextExecution) {
		MoneyFlow moneyFlow = new MoneyFlow();
		moneyFlow.setAmount(pendingMoneyFlow.getAmount());
		moneyFlow.setCategory(pendingMoneyFlow.getCategory());
		moneyFlow.setDescription(pendingMoneyFlow.getDescription());
		moneyFlow.setDate(recalculatedNextExecution);
		moneyFlow.setUser(pendingMoneyFlow.getUser());
		moneyFlow.setWallet(pendingMoneyFlow.getWallet());
		moneyFlow.setFinancialGoal(pendingMoneyFlow.getFinancialGoal());
		return moneyFlow;
	}
}
