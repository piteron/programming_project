package pl.polsl.pp.budget.web.controller.page.main;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.hibernate.Hibernate;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.data.filter.Filter;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.MoneyFlow;
import pl.polsl.pp.budget.domain.User;
import pl.polsl.pp.budget.domain.Wallet;

@Controller
@RequestMapping("/statistics")
@PreAuthorize(value = "isAuthenticated()")
public class StatisticsController {
	
	@Resource
	private User user;
	
	@Resource
	private ParameterizedFilter<Long> moneyFlowsByWalletId;
	
	@Resource
	private ParameterizedFilter<Long> usersWalletsByUserId;	
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String getStatisticsPage(Model model, Wallet chosenWallet) {
		List<Wallet> userWallets = usersWalletsByUserId.filter(user.getId());
		model.addAttribute("walletCounter", userWallets.size());
		
		List<BigDecimal> moneyInWallet = new ArrayList<>();
		List<BigDecimal> incameInWallet = new ArrayList<>();		
		List<BigDecimal> outcameInWallet = new ArrayList<>();
		List<BigDecimal> moneyInWalletMonth = new ArrayList<>();
		List<BigDecimal> incameInWalletMonth = new ArrayList<>();		
		List<BigDecimal> outcameInWalletMonth = new ArrayList<>();

		model.addAttribute("test", "testowa");
		model.addAttribute("userWallets", userWallets);
		
		BigDecimal totalMoneySum = new BigDecimal(0);
		
		Calendar today = Calendar.getInstance();
		

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -30);
		Date last10Days = cal.getTime();
		
		
		for(Wallet wallet : userWallets)
		{			
			addStatistics(wallet, moneyInWallet, incameInWallet, outcameInWallet);
			addStatistics(wallet, moneyInWalletMonth, incameInWalletMonth, outcameInWalletMonth, last10Days);			
		}
		
		for(BigDecimal money : moneyInWallet)
		{
			totalMoneySum = totalMoneySum.add(money);	
		}
		model.addAttribute("moneyInWallet", moneyInWallet);
		model.addAttribute("incameInWallet", incameInWallet);
		model.addAttribute("outcameInWallet", outcameInWallet);
		
		model.addAttribute("moneyInWalletMonth", moneyInWalletMonth);
		model.addAttribute("incameInWalletMonth", incameInWalletMonth);
		model.addAttribute("outcameInWalletMonth", outcameInWalletMonth);		
		
		model.addAttribute("totalMoneySum", totalMoneySum);
		return "statistics";
	}
	
	private void addStatistics(Wallet wallet, List<BigDecimal> walletSaldo, List<BigDecimal> walletIncame, List<BigDecimal> walletOutcame)
	{
		BigDecimal saldo = new BigDecimal(0);
		BigDecimal incame = new BigDecimal(0);
		BigDecimal outcame = new BigDecimal(0);
		
		List<MoneyFlow> moneyFlows = moneyFlowsByWalletId.filter(wallet.getId());	
		for(MoneyFlow flow : moneyFlows)
		{
			saldo = saldo.add(flow.getAmount());
			
			if(flow.getAmount().signum() == 1)
				incame = incame.add(flow.getAmount());

			if(flow.getAmount().signum() == -1)
				outcame = outcame.add(flow.getAmount());					
		}
		
		walletSaldo.add(saldo);
		walletIncame.add(incame);
		walletOutcame.add(outcame);		
	}
	
	private void addStatistics(Wallet wallet, List<BigDecimal> walletSaldo, List<BigDecimal> walletIncame, List<BigDecimal> walletOutcame, Date startDay )
	{
		BigDecimal saldo = new BigDecimal(0);
		BigDecimal incame = new BigDecimal(0);
		BigDecimal outcame = new BigDecimal(0);
		
		List<MoneyFlow> moneyFlows = moneyFlowsByWalletId.filter(wallet.getId());	
		for(MoneyFlow flow : moneyFlows)
		{
			if(flow.getDate().after(startDay))
			{
				saldo = saldo.add(flow.getAmount());
			
				if(flow.getAmount().signum() == 1)
					incame = incame.add(flow.getAmount());

				if(flow.getAmount().signum() == -1)
					outcame = outcame.add(flow.getAmount());
			}
		}
		
		walletSaldo.add(saldo);
		walletIncame.add(incame);
		walletOutcame.add(outcame);			
	}	

}
