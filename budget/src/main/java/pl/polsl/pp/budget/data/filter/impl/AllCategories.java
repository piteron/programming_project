package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.Filter;
import pl.polsl.pp.budget.domain.Category;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class AllCategories extends CriteriaCreator implements Filter {

	@Override
	public <RESULT> RESULT filter() {
		Criteria criteria = getNewCriteria();
		return Safe.cast(criteria.list());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return Category.class;
	}
}
