package pl.polsl.pp.budget.data;

import java.io.Serializable;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class UniversalDao extends HibernateSessionHolder {

    public void save(Object entity) {
        getSession().saveOrUpdate(entity);
    }

    public void delete(Object entity) {
        getSession().delete(entity);
    }
    
    public <MODEL, ID extends Serializable> MODEL load(Class<MODEL> modelClass, ID id) {
        return Safe.cast(getSession().load(modelClass, id));
    }
    public <MODEL, ID extends Serializable> MODEL get(Class<MODEL> modelClass, ID id) {
    	return Safe.cast(getSession().get(modelClass, id));
    }
}
