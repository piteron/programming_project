package pl.polsl.pp.budget.web.controller.page.main;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.hibernate.Hibernate;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.data.filter.Filter;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.FinancialGoal;
import pl.polsl.pp.budget.domain.MoneyFlow;
import pl.polsl.pp.budget.domain.User;
import pl.polsl.pp.budget.domain.Wallet;

@Controller
@RequestMapping("/money_flow")
@PreAuthorize(value = "isAuthenticated()")
public class MoneyFlowController {

	private final static String MONEY_FLOWS_PAGE_NAME = "moneyFlow";
	private final static String ADD_MONEY_FLOW_PAGE_NAME = "addMoneyFlow";
	private final static String MOVE_TO_FINANCIAL_GOAL_PAGE = "moveMoneyFlow";
	@Resource
	private User user;

	@Resource
	private ParameterizedFilter<Long> moneyFlowsByWalletId;
	
	@Resource
	private ParameterizedFilter<Long> usersWalletsByUserId;
	
	@Resource
	private Filter allCategories;
	
	@Resource
	private ParameterizedFilter<Long> financialGoalsByWalletId;
	
	@Resource
	private ParameterizedFilter<Long> financialGoalById;
	
	@Resource
	private UniversalDao dao;

	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
	@RequestMapping(method = RequestMethod.GET)
	public String getMoneyFlowPage(Model model, Wallet chosenWallet) {
		Map<Long, String> userWallets = getUserWallets();
		Wallet wallet;
		if(chosenWallet.getId() == null) {
			wallet = new Wallet();
			wallet.setId(userWallets.entrySet().iterator().next().getKey());
		} else {
			wallet = chosenWallet;
		}
		List<MoneyFlow> moneyFlows = moneyFlowsByWalletId.filter(wallet.getId());
		BigDecimal moneyFlowsSum = moneyFlows.stream().map(MoneyFlow::getAmount).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		model.addAttribute("wallet", wallet);
		model.addAttribute("wallets", userWallets);
		model.addAttribute("moneyFlows", moneyFlows);
		model.addAttribute("moneyFlowsSum", moneyFlowsSum);
		return MONEY_FLOWS_PAGE_NAME;
	}
	
	@RequestMapping(value = "delete/{id}")
	public String deleteMoneyFlow(Model model, @PathVariable Long id) {
		MoneyFlow moneyFlowToDelete = new MoneyFlow();
		moneyFlowToDelete.setId(id);
		dao.delete(moneyFlowToDelete);
		return "redirect:/money_flow";
	}
	
	@RequestMapping(params = "add", method = RequestMethod.GET)
	public String getMoneyFlowAddPage(Model model, @ModelAttribute Wallet wallet) {
		MoneyFlow newMoneyFlow = new MoneyFlow();
		newMoneyFlow.setDate(new Date());
		model.addAttribute("action", "money_flow/add");
		return prepareMoneyFlowPreparationPage(model, wallet, newMoneyFlow);
	}
	
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	@Transactional
	public String getMoneyFlowAddPage(Model model, @ModelAttribute Wallet wallet, @PathVariable Long id) {
		MoneyFlow moneyFlowToEdit = dao.load(MoneyFlow.class, id);
		Hibernate.initialize(moneyFlowToEdit);
		model.addAttribute("action", "");
		return prepareMoneyFlowPreparationPage(model, wallet, moneyFlowToEdit);
	}

	private String prepareMoneyFlowPreparationPage(Model model, Wallet wallet, MoneyFlow moneyFlow) {
		model.addAttribute("moneyFlow", moneyFlow);
		model.addAttribute("wallet", dao.get(Wallet.class, wallet.getId()));
		model.addAttribute("categories", allCategories.filter());
		return ADD_MONEY_FLOW_PAGE_NAME;
	}
	
	@RequestMapping(value = {"add", "edit/{id}"}, method = RequestMethod.POST)
	public String addMoneyFlow(Model model, @Valid MoneyFlow moneyFlow, Errors errors) {
		if(errors.hasErrors())
		{
			model.addAttribute("categories", allCategories.filter());
			return ADD_MONEY_FLOW_PAGE_NAME;
		}
		moneyFlow.setUser(user);
		dao.save(moneyFlow);
		return "redirect:/money_flow";
	}
	
	//@ModelAttribute("wallets")
	@Transactional
    public Map<Long, String> getUserWallets() {
    	List<Wallet> userWallets = usersWalletsByUserId.filter(user.getId());
    	return userWallets.stream().collect(Collectors.toMap(Wallet::getId, Wallet::getName,(v1, v2) -> null, LinkedHashMap::new));
    }
	
	@RequestMapping(value = "move/{id}", method = RequestMethod.GET)
	@Transactional
	public String moveToFinanacialGoal(Model model, @ModelAttribute Wallet wallet, @PathVariable Long id)
	{
		
		model.addAttribute("financialGoal", new FinancialGoal());
		Map<Long, String> finacialGoals =getFinancialGoalsByWallet(wallet);
		model.addAttribute("financialGoals", finacialGoals);
		return MOVE_TO_FINANCIAL_GOAL_PAGE;
		
	}
	
	@RequestMapping(value = "move/{id}", method = RequestMethod.POST)
	@Transactional
	public String moveToFinanacialGoal(Model model, @ModelAttribute FinancialGoal financialGoal , @PathVariable Long id)
	{
		FinancialGoal financianGoalToEdit =financialGoalById.filter(financialGoal.getId());
		Set <MoneyFlow> moneyFlowList = financianGoalToEdit.getMoneyFlows();
		MoneyFlow moneyFlowToEdit = dao.load(MoneyFlow.class, id);
		Hibernate.initialize(moneyFlowToEdit);
		moneyFlowList.add(moneyFlowToEdit);
		System.out.print(moneyFlowToEdit.getDescription());
		System.out.print(financianGoalToEdit.getName());
		financianGoalToEdit.setMoneyFlows(moneyFlowList);
		dao.save(financianGoalToEdit);
		return "redirect:/money_flow";
		
	}
	
	@Transactional
    public Map<Long, String> getFinancialGoalsByWallet(Wallet wallet) {
		List<FinancialGoal> finacialGoals = financialGoalsByWalletId.filter(wallet.getId());
    	return finacialGoals.stream().collect(Collectors.toMap(FinancialGoal::getId, FinancialGoal::getName,(v1, v2) -> null, LinkedHashMap::new));
    }
}
