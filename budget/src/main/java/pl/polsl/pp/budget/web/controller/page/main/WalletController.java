package pl.polsl.pp.budget.web.controller.page.main;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.data.filter.Filter;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.MoneyFlow;
import pl.polsl.pp.budget.domain.User;
import pl.polsl.pp.budget.domain.Wallet;

@Controller
@RequestMapping("/wallet")
@PreAuthorize(value = "isAuthenticated()")
public class WalletController {
	

	
	private final static String WALLET_PAGE_NAME = "wallet";
	private final static String ADD_WALLET_PAGE_NAME = "addWallet";
	
	@Resource
	private User user;
	
	@Resource
	private ParameterizedFilter<Long> walletById;
	
	@Resource
	private Filter allWallets;
	
	@Resource
	private ParameterizedFilter<Long> moneyFlowsByWalletId;
	
	
	@Resource
	private UniversalDao dao;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String getMoneyFlowPage(Model model) {
		List<Wallet> wallets = allWallets.filter();
		model.addAttribute("wallets", wallets);;
		return WALLET_PAGE_NAME;
	}
	
	@RequestMapping(value = "delete/{id}")
	public String deleteMoneyFlow(Model model, @PathVariable Long id) {
		Wallet walletToDelete=walletById.filter(id);
		dao.delete(walletToDelete);
		return "redirect:/wallet";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	String addCategories(Model model)
	{	
		model.addAttribute("wallet", new Wallet());
		return ADD_WALLET_PAGE_NAME;
	}
	
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	@Transactional
	String editCategories(Model model, @PathVariable Long id)
	{
		Wallet walletToEdit = walletById.filter(id);
		model.addAttribute("wallet", walletToEdit);
		return ADD_WALLET_PAGE_NAME;
	}
	
	@RequestMapping(value = {"add", "edit/{id}"}, method = RequestMethod.POST)
	public String addMoneyFlow(Model model, @Valid Wallet wallet, Errors errors) {
		if(errors.hasErrors())
		{
			return ADD_WALLET_PAGE_NAME;
		}
		wallet.setOwningUser(user);
		dao.save(wallet);
		return "redirect:/wallet";
	}
	
}
