package pl.polsl.pp.budget.function;

public class Safe {
	
	@SuppressWarnings("unchecked")
	public static <RESULT> RESULT cast(Object object) {
		return (RESULT) object;
	}
}
