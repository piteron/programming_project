package pl.polsl.pp.budget.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pl.polsl.pp.budget.domain.enumeration.CategoryType;

@Entity
public class Category {
	
	private Long id;
	private CategoryType categoryType;
	private String description;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull(message="Kategoria - nie może być pusta")
	@Enumerated(EnumType.STRING)
	public CategoryType getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(CategoryType categoryType) {
		this.categoryType = categoryType;
	}

	@NotNull(message="Opis - nie może być pusty")
	@Size(min=1,max=100, message="Opis - minimum 1 znak, maksymalnie 100")	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
