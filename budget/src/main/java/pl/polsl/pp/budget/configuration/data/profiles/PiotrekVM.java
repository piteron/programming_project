package pl.polsl.pp.budget.configuration.data.profiles;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import pl.polsl.pp.budget.configuration.data.HibernateConfiguration;

@Profile("piotrek")
@Configuration
@PropertySource(value = { "classpath:piotrek.properties" })
@Import(HibernateConfiguration.class)
public class PiotrekVM {

}
