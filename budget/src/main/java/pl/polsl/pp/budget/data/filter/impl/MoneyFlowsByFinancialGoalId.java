package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.MoneyFlow;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class MoneyFlowsByFinancialGoalId extends CriteriaCreator implements ParameterizedFilter<Long> {

	@Override
	public <RESULT> RESULT filter(Long financialGoal) {
		Criteria criteria = getNewCriteria();
		criteria.add(Restrictions.eq("financialGoal.id", financialGoal));
		return Safe.cast(criteria.list());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return MoneyFlow.class;
	}
}
