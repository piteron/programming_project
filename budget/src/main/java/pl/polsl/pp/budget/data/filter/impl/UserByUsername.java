package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.User;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class UserByUsername extends CriteriaCreator implements ParameterizedFilter<String> {

	@Override
	public <RESULT> RESULT filter(String login) {
		Criteria criteria = getNewCriteria();
		criteria.add(Restrictions.eq("login", login));
		return Safe.cast(criteria.uniqueResult());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return User.class;
	}

}
