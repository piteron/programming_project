package pl.polsl.pp.budget.data.filter;

public interface Filter {

	<RESULT> RESULT filter();
}
