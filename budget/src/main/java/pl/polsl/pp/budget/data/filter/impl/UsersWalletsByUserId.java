package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.Wallet;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class UsersWalletsByUserId extends CriteriaCreator implements ParameterizedFilter<Long> {

	@Override
	public <RESULT> RESULT filter(Long userId) {
		Criteria criteria = getNewCriteria();
		criteria.createAlias("foreignUsers", "foreignUser", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.or(Restrictions.eq("owningUser.id", userId), Restrictions.eq("foreignUser.id", userId)));	
		criteria.addOrder(Order.asc("name"));
		return Safe.cast(criteria.list());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return Wallet.class;
	}

}
