package pl.polsl.pp.budget.configuration.data;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "pl.polsl.pp.budget.data" })
public class HibernateConfiguration {

	@Resource
	Environment env;

	@Bean
	public DataSource dataSource() {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		try {
			dataSource.setDriverClass(env.getRequiredProperty("jdbc.driverClassName"));
		} catch (PropertyVetoException e) { //TODO aplikacja nie powinna startować?
			e.printStackTrace();
		}
		dataSource.setJdbcUrl(env.getRequiredProperty("jdbc.url"));
        dataSource.setUser(env.getRequiredProperty("jdbc.user"));
        dataSource.setPassword(env.getRequiredProperty("jdbc.pass"));
        //dataSource.setMinPoolSize();
		//dataSource.setMaxPoolSize();
		//dataSource.getAcquireIncrement();
		return dataSource;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
	       LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	        sessionFactory.setDataSource(dataSource());
	        sessionFactory.setPackagesToScan(new String[] { "pl.polsl.pp.budget.domain"});
	        sessionFactory.setHibernateProperties(hibernateProperties());
	        return sessionFactory;
	}

	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(s);
		return transactionManager;
	}

	@SuppressWarnings("serial")
	Properties hibernateProperties() {
		return new Properties() {
			{
				setProperty("hibernate.dialect");
				//setProperty("hibernate.globally_quoted_identifiers", "true");
		        //setProperty("hibernate.show_sql");
		        setProperty("hibernate.format_sql");
		        setProperty("hibernate.default_schema");
		        setProperty("hibernate.hbm2ddl.auto");
		        setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
			}
			
			private void setProperty(String property) {
				String propertyFromEnvironment = env.getProperty(property);
				if(propertyFromEnvironment == null) {
					return;
				}
				setProperty(property, propertyFromEnvironment);
			}
		};
	}
}
