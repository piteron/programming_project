package pl.polsl.pp.budget.web.controller.page.main;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.hibernate.Hibernate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.FinancialGoal;
import pl.polsl.pp.budget.domain.MoneyFlow;
import pl.polsl.pp.budget.domain.User;
import pl.polsl.pp.budget.domain.Wallet;

@Controller
@RequestMapping("/financialGoal")
@PreAuthorize(value = "isAuthenticated()")
public class FinanacialGoalController {
	
	final static String FINANCIAL_GOAL_PAGE_NAME = "financialGoal";
	final static String FINANCIAL_GOAL_DETALIS_PAGE_NAME = "financialGoalDetalis";
	final static String ADD_FINANCIAL_GOAL_PAGE_NAME = "addFinancialGoal";


	@Resource
	private User user;
	
	
	@Resource
	private ParameterizedFilter<Long> financialGoalsByWalletId;
	
	@Resource
	private ParameterizedFilter<Long> usersWalletsByUserId;
	
	@Resource
	private ParameterizedFilter<Long> moneyFlowsByFinancialGoalId;
	
	@Resource
	private ParameterizedFilter<Long> financialGoalById;
	
	@Resource
	private UniversalDao dao;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getFinancialGoalPage(Model model, Wallet chosenWallet) {
		Map<Long, String> userWallets = getUserWallets();
		Wallet wallet;
		if(chosenWallet.getId() == null) {
			wallet = new Wallet();
			wallet.setId(userWallets.entrySet().iterator().next().getKey());
		} else {
			wallet = chosenWallet;
		}
		List<FinancialGoal> financialGoals = financialGoalsByWalletId.filter(wallet.getId());
		model.addAttribute("wallet", wallet);
		model.addAttribute("wallets", userWallets);
		model.addAttribute("financialGoals", financialGoals);
		return FINANCIAL_GOAL_PAGE_NAME;
	}
	
	@RequestMapping(value = "detalis/{id}")
	public String deleteFinancialGoal(Model model, @PathVariable Long id) {
		List<MoneyFlow> moneyFlows = moneyFlowsByFinancialGoalId.filter(id);
		model.addAttribute("moneyFlows", moneyFlows);
		FinancialGoal finacialGoal=financialGoalById.filter(id);
		model.addAttribute("moneyFlows", moneyFlows);
		model.addAttribute("financialGoal", finacialGoal);
		return FINANCIAL_GOAL_DETALIS_PAGE_NAME;
	}
	
	@Transactional
    public Map<Long, String> getUserWallets() {
    	List<Wallet> userWallets = usersWalletsByUserId.filter(user.getId());
    	return userWallets.stream().collect(Collectors.toMap(Wallet::getId, Wallet::getName,(v1, v2) -> null, LinkedHashMap::new));
    }
	
	@RequestMapping(value = {"add", "edit/{id}"}, method = RequestMethod.POST)
	public String addFinancialGoal(Model model, @Valid FinancialGoal financialGoal, Errors errors) {
		if(errors.hasErrors())
		{
			return ADD_FINANCIAL_GOAL_PAGE_NAME;
		}
		dao.save(financialGoal);
		return "redirect:/financialGoal";
	}
	
	@RequestMapping(params = "add", method = RequestMethod.GET)
	public String getFinancialGoalAddPage(Model model, @ModelAttribute Wallet wallet) {
		FinancialGoal newFinancialGoal = new FinancialGoal();
		model.addAttribute("action", "financialGoal/add");
		model.addAttribute("wallet", dao.get(Wallet.class, wallet.getId()));
		model.addAttribute("financialGoal", newFinancialGoal);
		return ADD_FINANCIAL_GOAL_PAGE_NAME;
	}
	
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	@Transactional
	public String getFinancialGoalAddPage(Model model, @ModelAttribute Wallet wallet, @PathVariable Long id) {
		FinancialGoal financialGoalToEdit = financialGoalById.filter(id);
		model.addAttribute("action", "");
		model.addAttribute("financialGoal", financialGoalToEdit);
		return ADD_FINANCIAL_GOAL_PAGE_NAME;
	}
	
	
}
