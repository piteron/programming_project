package pl.polsl.pp.budget.web.controller.page.main;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.data.filter.Filter;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.Category;

@Controller
@RequestMapping("/category")
@PreAuthorize(value = "isAuthenticated()")
public class CategoryController {

	private final static String CATEGORY_PAGE_NAME = "category";
	private final static String ADD_CATEGORY_PAGE_NAME = "addCategory";
	@Resource
	private Filter allCategories;
	
	@Resource
	private ParameterizedFilter<Long> categoryById;
	
	@Resource
	private UniversalDao dao;
	
	@RequestMapping(method = RequestMethod.GET)
	String viewCategories(Model model)
	{
		model.addAttribute("categories", allCategories.filter());
		return CATEGORY_PAGE_NAME;
	}
	
		
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	String addCategories(Model model)
	{	
		model.addAttribute("category", new Category());
		return ADD_CATEGORY_PAGE_NAME;
	}
	
	@RequestMapping(value = "delete/{id}")
	String delCategories(Model model, @PathVariable Long id)
	{	
		Category categoryToDelete=new Category();
		categoryToDelete.setId(id);
		dao.delete(categoryToDelete);
		return "redirect:/category";

	}
	
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	@Transactional
	String editCategories(Model model, @PathVariable Long id)
	{
		Category categoryToEdit = categoryById.filter(id);
		model.addAttribute("category", categoryToEdit);
		return ADD_CATEGORY_PAGE_NAME;
	}
	
	@RequestMapping(value = {"add", "edit/{id}"}, method = RequestMethod.POST)
	public String addMoneyFlow(Model model, @Valid Category category, Errors errors) {
		if(errors.hasErrors())
		{
			return ADD_CATEGORY_PAGE_NAME;
		}
		dao.save(category);
		return "redirect:/category";
	}
}
