package pl.polsl.pp.budget.data.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;

import pl.polsl.pp.budget.data.HibernateSessionHolder;

public abstract class CriteriaCreator extends HibernateSessionHolder {

	protected Criteria getNewCriteria() {
		return getNewCriteriaForRootAlias(CriteriaSpecification.ROOT_ALIAS);
	}
	
	protected Criteria getNewCriteriaForRootAlias(String rootAlias) {
		return getSession().createCriteria(getCriteriaClass(), rootAlias);
	}

	protected abstract Class<?> getCriteriaClass();
}
