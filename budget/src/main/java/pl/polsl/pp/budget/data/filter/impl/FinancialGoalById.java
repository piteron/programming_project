package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.FinancialGoal;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class FinancialGoalById extends CriteriaCreator implements ParameterizedFilter<Long> {

	@Override
	public <RESULT> RESULT filter(Long id) {
		Criteria criteria = getNewCriteria();
		criteria.add(Restrictions.eq("id", id));
		return Safe.cast(criteria.uniqueResult());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return FinancialGoal.class;
	}
}
