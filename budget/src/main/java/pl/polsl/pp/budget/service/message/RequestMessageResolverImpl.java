package pl.polsl.pp.budget.service.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Service;

@Service
public class RequestMessageResolverImpl implements RequestMessageResolver {

    @Autowired
    private MessageSource messageSource;

    @Override
    public String resolve(String code) {
        return messageSource.getMessage(new DefaultMessageSourceResolvable(code), null);
    }
}
