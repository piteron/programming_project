package pl.polsl.pp.budget.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import pl.polsl.pp.budget.domain.enumeration.UserType;

@Entity
public class User {

	private Long id;
	private UserType type;
	private String login;
	private String password;
	private Set<Wallet> ownedWallets;
	private Set<Wallet> foreignWallets;
	private Set<MoneyFlow> moneyFlows; 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@OneToMany(mappedBy = "owningUser")
	public Set<Wallet> getOwnedWallets() {
		return ownedWallets;
	}

	public void setOwnedWallets(Set<Wallet> ownedWallets) {
		this.ownedWallets = ownedWallets;
	}

	@ManyToMany(mappedBy = "foreignUsers")
	public Set<Wallet> getForeignWallets() {
		return foreignWallets;
	}

	public void setForeignWallets(Set<Wallet> foreignWallets) {
		this.foreignWallets = foreignWallets;
	}

	@OneToMany(mappedBy = "user")
	public Set<MoneyFlow> getMoneyFlows() {
		return moneyFlows;
	}

	public void setMoneyFlows(Set<MoneyFlow> moneyFlows) {
		this.moneyFlows = moneyFlows;
	}
}
