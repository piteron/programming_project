package pl.polsl.pp.budget.service;


public interface Resolver<ITEM> {

    ITEM resolve();
}
