package pl.polsl.pp.budget.web.controller.page.main;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = {"/myPage"})
public class MyPageController {

    @PreAuthorize(value = "isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET)
    public String myPage(Model model) {
        return "myPage";
    }

}
