package pl.polsl.pp.budget.domain;

import javax.persistence.Entity;

@Entity
public class PendingMoneyFlow extends MoneyFlowTemplate {

	private String cronExpression;

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
}
