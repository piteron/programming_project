package pl.polsl.pp.budget.cofiguration.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.util.PathMatcher;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import pl.polsl.pp.budget.configuration.service.ServiceConfiguration;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "pl.polsl.pp.budget.web", "pl.polsl.pp.budget.configuration.data.profiles" })
@Import(value = { ServiceConfiguration.class, MessageSourceConfiguration.class })
public class MvcConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	private MessageSource messageSource;

	@Bean
	public UrlBasedViewResolver jspViewResolver() {
		UrlBasedViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}

	@Bean
	public PathMatcher pathMatcher() {
		return new CaseInsensitivePathMatcher();
	}

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		configurer.setPathMatcher(pathMatcher());
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
		validatorFactoryBean.setValidationMessageSource(messageSource);
		return validatorFactoryBean;
	}

	@Override
	public Validator getValidator() {
		return validator();
	}
	
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/resources/**")
                    .addResourceLocations("/resources/");
    }
    
}
