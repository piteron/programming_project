package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.Category;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class CategoryById extends CriteriaCreator implements ParameterizedFilter<Long> {

	@Override
	public <RESULT> RESULT filter(Long userId) {
		Criteria criteria = getNewCriteria();
		criteria.add(Restrictions.eq("id", userId));
		return Safe.cast(criteria.uniqueResult());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return Category.class;
	}
}
