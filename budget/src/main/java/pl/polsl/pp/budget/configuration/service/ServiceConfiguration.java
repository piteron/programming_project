package pl.polsl.pp.budget.configuration.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = { "pl.polsl.pp.budget.service" })
public class ServiceConfiguration {
}
