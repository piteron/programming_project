package pl.polsl.pp.budget.domain.enumeration;

public enum UserType {
	ADMIN,
	USER
}
