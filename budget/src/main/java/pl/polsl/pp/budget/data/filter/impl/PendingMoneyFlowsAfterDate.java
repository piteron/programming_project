package pl.polsl.pp.budget.data.filter.impl;

import java.util.Date;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.PendingMoneyFlow;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class PendingMoneyFlowsAfterDate extends CriteriaCreator implements ParameterizedFilter<Date> {

	@Override
	public <RESULT> RESULT filter(Date date) {
		Criteria criteria = getNewCriteria();
		criteria.setFetchMode("category", FetchMode.SELECT);
		criteria.setFetchMode("user", FetchMode.SELECT);
		criteria.setFetchMode("wallet", FetchMode.SELECT);
		criteria.setFetchMode("financialGoal", FetchMode.SELECT);
		criteria.add(Restrictions.lt("date", date));
		return Safe.cast(criteria.list());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return PendingMoneyFlow.class;
	}

}
