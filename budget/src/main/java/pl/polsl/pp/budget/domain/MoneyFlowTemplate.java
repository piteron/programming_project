package pl.polsl.pp.budget.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

@MappedSuperclass
public class MoneyFlowTemplate {

	protected Long id;
	private BigDecimal amount;
	private String description;
	private Date date;
	private Category category;
	private User user;
	private Wallet wallet;
	private FinancialGoal financialGoal;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull(message="Kwota - nie może być pusta")
	@Digits(integer=6, fraction=2, message="Kwota - Maksymalnie 6 cyfr z dokładnością do 2 miejsc po przecinku")
	@Min(value=1, message="Kwota - musi być większa lub równa 1")
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@NotNull(message="Opis - nie może być pusty")
	@Size(min=1,max=100, message="Opis - minimum 1 znak, maksymalnie 100")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull(message="Data - nie może być pusta")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@ManyToOne
	@PrimaryKeyJoinColumn
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne
	@PrimaryKeyJoinColumn
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	@ManyToOne
	@PrimaryKeyJoinColumn
	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	@ManyToOne
	@PrimaryKeyJoinColumn
	public FinancialGoal getFinancialGoal() {
		return financialGoal;
	}

	public void setFinancialGoal(FinancialGoal financialGoal) {
		this.financialGoal = financialGoal;
	}

}
