package pl.polsl.pp.budget.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Wallet {

	private Long id;
	private User owningUser;
	private String name;
	private Set<User> foreignUsers;
	private Set<MoneyFlow> moneyFlows;
	private Set<FinancialGoal> financialGoals;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@PrimaryKeyJoinColumn
	public User getOwningUser() {
		return owningUser;
	}

	public void setOwningUser(User owningUser) {
		this.owningUser = owningUser;
	}

	@NotNull
	@Size(min=1,max=100, message="Nazwa porfela - minimum 1 znak, maksymalnie 100")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany
	@JoinTable(name = "wallet_user", joinColumns = @JoinColumn(referencedColumnName = "id", name = "wallet_id") , inverseJoinColumns = @JoinColumn(referencedColumnName = "id", name = "user_id") )
	public Set<User> getForeignUsers() {
		return foreignUsers;
	}

	public void setForeignUsers(Set<User> foreignUsers) {
		this.foreignUsers = foreignUsers;
	}

	@OneToMany(mappedBy = "wallet")
	public Set<MoneyFlow> getMoneyFlows() {
		return moneyFlows;
	}

	public void setMoneyFlows(Set<MoneyFlow> moneyFlows) {
		this.moneyFlows = moneyFlows;
	}

	@OneToMany(mappedBy = "wallet")
	public Set<FinancialGoal> getFinancialGoals() {
		return financialGoals;
	}

	public void setFinancialGoals(Set<FinancialGoal> financialGoals) {
		this.financialGoals = financialGoals;
	}
}
