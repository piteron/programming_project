package pl.polsl.pp.budget.web.service.session;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Service;

import pl.polsl.pp.budget.dto.LoggedUser;

@Service
public class AuthenticationListener implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {

    @Autowired
    private SessionUserHolder sessionUserHolder;

    @Autowired
    private HttpSession session;

    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event) {
        LoggedUser principal = (LoggedUser) event.getAuthentication().getPrincipal();
        sessionUserHolder.setUser(principal.getUser());
    }
}
