package pl.polsl.pp.budget.data.filter.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.polsl.pp.budget.data.filter.CriteriaCreator;
import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.FinancialGoal;
import pl.polsl.pp.budget.function.Safe;

@Repository
@Transactional
public class FinancialGoalsByWalletId extends CriteriaCreator implements ParameterizedFilter<Long> {

	@Override
	public <RESULT> RESULT filter(Long walletId) {
		Criteria criteria = getNewCriteria();
		criteria.add(Restrictions.eq("wallet.id", walletId));
		return Safe.cast(criteria.list());
	}

	@Override
	protected Class<?> getCriteriaClass() {
		return FinancialGoal.class;
	}
}
