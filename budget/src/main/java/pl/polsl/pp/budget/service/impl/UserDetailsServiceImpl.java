package pl.polsl.pp.budget.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pl.polsl.pp.budget.data.filter.ParameterizedFilter;
import pl.polsl.pp.budget.domain.User;
import pl.polsl.pp.budget.dto.LoggedUser;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ParameterizedFilter<String> userByUsername;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userByUsername.filter(email);
        if (user == null) throw new UsernameNotFoundException("Nie znaleziono uzytkownika");
        UserDetails userDetails = new LoggedUser(user);
        return userDetails;
    }
}
