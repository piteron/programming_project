package pl.polsl.pp.budget.service.impl;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;

@Service
public class MockUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	if(!"admin".equals(username)) {
    		throw new UsernameNotFoundException("nima");
    	}
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_ADMIN");
        Collection<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(authority);
        // hasło: admin
        return new User("admin", "$2a$10$Xmud6hqK1DMLdw5T2NqW/.ZXoNzz4iGbO1KlDB9hA3c1Pcrj7OnLu", grantedAuthorities);
    }

}
