package pl.polsl.pp.budget.domain.enumeration;

public enum CategoryType {
	INCOME,
	EXPENSE
}
