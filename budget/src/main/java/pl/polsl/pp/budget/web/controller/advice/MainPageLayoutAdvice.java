package pl.polsl.pp.budget.web.controller.advice;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import pl.polsl.pp.budget.dto.MenuItem;
import pl.polsl.pp.budget.service.message.RequestMessageResolver;

@ControllerAdvice(basePackages = {"pl.polsl.pp.budget.web.controller.page.main"})
public class MainPageLayoutAdvice {

	@Resource
    private RequestMessageResolver messageResolver;

    @ModelAttribute("pageMenu")
    @PreAuthorize(value = "isAuthenticated()")
    public List<MenuItem> getIndexMenu() {
        List<MenuItem> indexMenu = new ArrayList<>();
        indexMenu.add(new MenuItem(messageResolver.resolve("label.menu.user.dochodywydatki"), "money_flow"));
        indexMenu.add(new MenuItem(messageResolver.resolve("label.menu.user.usunkonto"), "http://www.onet.pl"));
//        if(UserType.ADMIN.equals(sessionUserHolder.getUser().getType())) {
//        	indexMenu.add(new MenuItem(messageResolver.resolve("label.menu.admin.jestemadminem"), "http://www.onet.pl"));
//        }
        return indexMenu;
    }
    
    
}
