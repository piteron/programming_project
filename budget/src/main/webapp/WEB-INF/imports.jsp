<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel='stylesheet' href='./webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
<link rel='stylesheet' href='./webjars/bootstrap/3.2.0/css/bootstrap-theme.min.css'>
<link rel='stylesheet' type="text/css" title="customCSS" href="./resources/css/customCSS.css"/>
<script type='text/javascript' src='./webjars/jquery/2.1.1/jquery.min.js'></script>
<script type='text/javascript' src='./webjars/bootstrap/3.2.0/js/bootstrap.min.js'></script>
