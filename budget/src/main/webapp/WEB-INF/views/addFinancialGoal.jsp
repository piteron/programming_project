<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/custom.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/menuCSS.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/addMoneyFlowCSS.css" />" >
<img src="<c:url value="/resources/mamonya.jpg" />" alt="" />   

<ul id="menu-bar">
   <li><a href="${jdbc.url}\budget\money_flow"><i></i>Dochody i wydatki</a></li>
   <li><a href="${jdbc.url}\budget\category"><i></i>Kategorie</a>
        <ul>
           <li><a href="${jdbc.url}\budget\category\add?">Dodaj kategorie</a></li>
       </ul>
   </li>
   <li  class="active"><a href="${jdbc.url}\budget\wallet"><i></i>Portfel</a>
       <ul>
           <li><a href="${jdbc.url}\budget\wallet\add?">Dodaj portfel</a></li>
       </ul>
   </li>
   <li><a href="${jdbc.url}\budget\statistics"><i></i>Statystyki</a></li> 
   <li><a href="${label.menu.user.usunkonto}"><i></i>Usuń konto</a></li>
</ul>

<div id="moneyFlowContener">
<spring:message code="label.form.wallet"/> ${wallet.name}
<br>
<form:form modelAttribute="financialGoal" action="${action}" method="POST">
	<form:errors path="*" element="div"  cssClass="addError"/>
	<label for="category">Nazwa:</label>
	<form:input type="text" path="name"/>
	<form:input type="hidden" path="wallet.id" value="${wallet.id}"/>
	<input type="submit" value="Zapisz" />
</form:form>
</div>