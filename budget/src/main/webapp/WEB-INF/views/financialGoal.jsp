<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:forEach items="${pageMenu}" var="menu">
       <li><a href="${menu.value}"><i></i>${menu.label}</a></li>
</c:forEach>

<form:form modelAttribute="wallet" action="financialGoal" method="GET">
	<input type="submit" name="add" value="Dodaj cel finansowy" />
	<form:select path="id" items="${wallets}" onchange="this.form.submit();"/>
</form:form>
<br>
<table>
	<c:forEach items="${financialGoals}" var="financialGoal">
		<tr>
			<td>
				<c:url var="detalisUrl" value="financialGoal/detalis/${financialGoal.id}" /> 
				<form:form action="${detalisUrl}" method="GET">
					<input type="submit" value="${financialGoal.name}" />
				</form:form>
			</td>
			<td>
				<c:url var="deleteUrl" value="money_flow/delete/${financialGoal.id}" /> 
				<form:form action="${deleteUrl}" method="GET">
					<input type="submit" onClick="return confirm('sure?')" value="Usuń" />
				</form:form>
			</td>
			<td>
				<c:url var="editUrl" value="money_flow/edit/${financialGoal.id}" /> 
				<form:form action="${editUrl}" method="GET">
					<input type="submit" value="Edytuj" />
				</form:form>
			</td>
		<tr>
	</c:forEach>
</table>