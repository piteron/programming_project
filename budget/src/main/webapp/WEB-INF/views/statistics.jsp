<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/custom.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/menuCSS.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/moneyFlowCSS.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/statistics.css" />" >
<img src="<c:url value="/resources/mamonya.jpg" />" alt="" />   

<ul id="menu-bar">
   <li><a href="${jdbc.url}\budget\money_flow"><i></i>Dochody i wydatki</a></li>
   <li><a href="${jdbc.url}\budget\category"><i></i>Kategorie</a>
        <ul>
           <li><a href="${jdbc.url}\budget\category\add?">Dodaj kategorie</a></li>
       </ul>
   </li>
   <li  ><a href="${jdbc.url}\budget\wallet"><i></i>Portfel</a>
       <ul>
           <li><a href="${jdbc.url}\budget\wallet\add?">Dodaj portfel</a></li>
       </ul>
   </li>
   <li class="active"><a href="${jdbc.url}\budget\statistics"><i></i>Statystyki</a></li>   
   <li><a href="${label.menu.user.usunkonto}"><i></i>Usuń konto</a></li>
   
</ul>

<div id="moneyFlowContener">
<spring:message code="label.form.wallet"/> ${wallet.name}
<br>
	<table>
	<tr><td>
		<table id="statistic">
			<tr>
				<td colspan="2">Ogólne:</td></td>
			</tr>		
			<tr>
				<td> Saldo(wszystkie protfele):</td><td>${totalMoneySum}</td>
			</tr>
			<tr>
				<td> Ilość posiadanych portfeli:</td><td>${userWallets.size()}</td>
			</tr>
		</table>
	</td></tr>	
	
	<tr><td>
		<table  id="statistic">
			<tr>
				<td> Saldo portfeli:</td>
			</tr>		
			<tr>
				<td colspan="2">
					<table>
						<tr><td>Lp.</td><td>Nazwa</td><td>Saldo</td><td>Przychód</td><td>Wydatki</td></tr>	
						<c:set var="iterator" value="0"/>
						<c:forEach items="${userWallets}" var="wallet">
							<tr<c:if test="${iterator%2==0}"> class=striped</c:if>><td>${iterator+1}</td><td>${wallet.getName()}</td><td>${moneyInWallet[iterator]}</td><td>${incameInWallet[iterator]}</td><td>${outcameInWallet[iterator]}</td></tr>
							<c:set var="iterator" value="${iterator+1}"/>	
						</c:forEach>
					</table>
								
				</td>
			</tr>
		</table>
	</td></tr>
	
	<tr><td>
		<table  id="statistic">
			<tr>
				<td> Saldo portfeli z ostatnich 30 dni:</td></td>
			</tr>		
			<tr>
				<td colspan = "2">
				
					<table>
						<tr><td>Lp.</td><td>Nazwa</td><td>Saldo</td><td>Przychód</td><td>Wydatki</td></tr>	
						<c:set var="iterator" value="0"/>
						<c:forEach items="${userWallets}" var="wallet">
							<tr<c:if test="${iterator%2==0}"> class=striped</c:if>><td>${iterator+1}</td><td>${wallet.getName()}</td><td>${moneyInWalletMonth[iterator]}</td><td>${incameInWalletMonth[iterator]}</td><td>${outcameInWalletMonth[iterator]}</td></tr>
							<c:set var="iterator" value="${iterator+1}"/>	
						</c:forEach>
					</table>
								
				</td>
			</tr>
		</table>
	</td></tr>
		
	</table>
</div>