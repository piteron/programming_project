<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../imports.jsp" %>

<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/custom.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/loginCSS.css" />" >
<img src="<c:url value="/resources/mamonya.jpg" />" alt="" />

<div class="container">
    <div class="row marketing loginTable">
        <form name='loginForm' action="<c:url value='j_spring_security_check' />" method='POST'>
            <div class="col-lg-6 leftSideLogin">
                <h4 class="labelLogin">Budget</h4>
                <hr class="hrLogin"/>
                <c:if test="${param.error != null}">
                    <div class="form-group has-error">
                        <span class="help-block">Login i hasło nie zgadzają się</span>
                    </div>
                </c:if>
                <label for="username" class="sr-only">Login</label>
                <input name='username' id="username" class="form-control inputsLoginPage" placeholder="Wpisz swój login" required autofocus>
                <label for="password" class="sr-only">Hasło</label>
                <input type="password" name='password' id="password" class="form-control inputsLoginPage" placeholder="Hasło" required>
                <button class="btn btn-primary loginButton" type="submit">Zaloguj</button>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </div>
        </form>
    </div>
</div>