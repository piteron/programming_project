<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/menuCSS.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/custom.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/moneyFlowCSS.css" />" >
<img src="<c:url value="/resources/mamonya.jpg" />" alt="" />   

<ul id="menu-bar">
   <li><a href="${jdbc.url}\budget\money_flow"><i></i>Dochody i wydatki</a></li>
   <li class="active"><a href="${jdbc.url}\budget\category"><i></i>Kategorie</a>
        <ul>
           <li><a href="${jdbc.url}\budget\category\add?">Dodaj kategorie</a></li>
       </ul>
   </li>
   <li  ><a href="${jdbc.url}\budget\wallet"><i></i>Portfel</a>
       <ul>
           <li><a href="${jdbc.url}\budget\wallet\add?">Dodaj portfel</a></li>
       </ul>
   </li>
   <li><a href="${jdbc.url}\budget\statistics"><i></i>Statystyki</a></li> 
   <li><a href="${label.menu.user.usunkonto}"><i></i>Usuń konto</a></li>
</ul>

<h1>Cel finansowy: ${financialGoal.name}</h1>
<c:set var="iterator" value="0"/>
<table>
 	<c:forEach items="${moneyFlows}" var="moneyFlow">
	<c:set var="iterator" value="${iterator+1}"/>
		<tr <c:if test="${iterator%2==0}"> class=striped</c:if>>
			<td align="right"><fmt:formatNumber value="${moneyFlow.amount}" maxFractionDigits="2" />zł</td>
			<td>${moneyFlow.date}</td>
			<td>${moneyFlow.description}</td>
			<td>${moneyFlow.category.description}</td>
		</tr>
	</c:forEach>
</table>