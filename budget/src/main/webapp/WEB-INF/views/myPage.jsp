<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/menuCSS.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/custom.css" />" >
<img src="<c:url value="/resources/mamonya.jpg" />" alt="" />

<ul id="menu-bar">
   <li><a href="${jdbc.url}\budget\money_flow"><i></i>Dochody i wydatki</a></li>
   <li><a href="${jdbc.url}\budget\category"><i></i>Kategorie</a>
        <ul>
           <li><a href="${jdbc.url}\budget\category\add?">Dodaj kategorie</a></li>
       </ul>
   </li>
   <li><a href="${jdbc.url}\budget\wallet"><i></i>Portfel</a>
       <ul>
           <li><a href="${jdbc.url}\budget\wallet\add?">Dodaj portfel</a></li>
       </ul>
   </li>
   <li><a href="${jdbc.url}\budget\statistics"><i></i>Statystyki</a></li> 
   <li><a href="${label.menu.user.usunkonto}"><i></i>Usuń konto</a></li>
</ul>