<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/custom.css" />" >
<link rel="Stylesheet" type="text/css" href="<c:url value="/resources/menuCSS.css" />" >
<img src="<c:url value="/resources/mamonya.jpg" />" alt="" />   

<ul id="menu-bar">
   <li><a href="${jdbc.url}\budget\money_flow"><i></i>Dochody i wydatki</a></li>
   <li><a href="${jdbc.url}\budget\category"><i></i>Kategorie</a>
        <ul>
           <li><a href="${jdbc.url}\budget\category\add?">Dodaj kategorie</a></li>
       </ul>
   </li>
   <li  class="active"><a href="${jdbc.url}\budget\wallet"><i></i>Portfel</a>
       <ul>
           <li><a href="${jdbc.url}\budget\wallet\add?">Dodaj portfel</a></li>
       </ul>
   </li>
   <li><a href="${jdbc.url}\budget\statistics"><i></i>Statystyki</a></li> 
   <li><a href="${label.menu.user.usunkonto}"><i></i>Usuń konto</a></li>
</ul>
<br>
<br>
<form:form modelAttribute="wallet" action="wallet/add" method="GET">
	<input type="submit" value="Dodaj portfel" />
</form:form>
<table>
		<c:forEach items="${wallets}" var="wallet">
		<tr>
			<td>
				${wallet.name}
			</td>
			<td>
				<form:form  modelAttribute="wallet" action="money_flow" method="GET">
					<input type="hidden" name="id" value="${wallet.id}">
					<input type="submit" value="Dochody i wydatki" />
				</form:form>
			</td>
			<td>			
				<c:url var="deleteUrl" value="wallet/delete/${wallet.id}" /> 
				<form:form action="${deleteUrl}" method="GET">
					<input type="submit" onClick="return confirm('sure?')" value="Usuń" />
				</form:form>
			</td>
			<td>
				<c:url var="editUrl" value="wallet/edit/${wallet.id}" /> 
				<form:form action="${editUrl}" method="GET">
					<input type="submit" value="Edytuj" />
				</form:form>
			</td>
		<tr>
	</c:forEach>
</table>