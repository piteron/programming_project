package pl.polsl.pp.budget;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.polsl.pp.budget.configuration.data.profiles.PiotrekVM;
import pl.polsl.pp.budget.configuration.service.ServiceConfiguration;
import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("piotrek")
@ContextConfiguration(classes = { PiotrekVM.class, ServiceConfiguration.class })
public class CreateSelectDeleteTest {

	@Autowired
	private UniversalDao dao;

	@Test
	public void testCreateUser() {
		User user = new User();
		user.setId(11L);
		user.setLogin("jan.nowak@gmail.com");
		//user.setPassword("$2a$10$ob9HJxRin6Arn8gKHFx9jeUhhrWChHJsPh42mP7DhlVh7ibFj8MQO");
		dao.save(user);

	}

	@Test
	@Transactional
	public void testGetUserById() throws Exception {

		User user = dao.load(User.class, 2L);

		assertNotNull(user);
	}

	@Test
	@Transactional
	public void testDeleteUser() throws Exception {
		User user = new User();
		user.setId(10L);
		dao.delete(user);

	}

}