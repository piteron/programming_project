package pl.polsl.pp.budget;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.polsl.pp.budget.configuration.data.profiles.PiotrekVM;
import pl.polsl.pp.budget.configuration.service.ServiceConfiguration;
import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("piotrek")
@ContextConfiguration(classes = { PiotrekVM.class, ServiceConfiguration.class })
public class CreateSelectDeleteUserTest {

	@Autowired
	private UniversalDao dao;

	@Test
	@Transactional
	public void testCreateUser() throws Exception {
		User user = new User();
		user.setId(10L);
		user.setLogin("jan.nowak@gmail.com");
		dao.save(user);		
		User user1 = dao.load(User.class, 10L);
		assertEquals("jan.nowak@gmail.com", user1.getLogin());
		
	}

	@Test
	@Transactional
	public void testGetUserById() throws Exception {

		User user = dao.load(User.class, 2L);

		assertNotNull(user);
	}


	@Test
	@Transactional
	public void testDeleteUser() throws Exception {
		User user = new User();
		user.setId(10L);
		dao.delete(user);
	
	}

}