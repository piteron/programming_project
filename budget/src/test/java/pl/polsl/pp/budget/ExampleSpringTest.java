package pl.polsl.pp.budget;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.polsl.pp.budget.configuration.data.profiles.PiotrekVM;
import pl.polsl.pp.budget.configuration.service.ServiceConfiguration;
import pl.polsl.pp.budget.data.UniversalDao;
import pl.polsl.pp.budget.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("piotrek")
@ContextConfiguration(classes = {PiotrekVM.class, ServiceConfiguration.class})
public class ExampleSpringTest {
	
	@Autowired
	private UniversalDao dao;
	
	@Test
	@Transactional
	public void test() {
		System.out.println(dao.load(User.class, 1L));
	}
}