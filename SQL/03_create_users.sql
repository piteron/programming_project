insert into category values(DEFAULT, 'INCOME', 'Wynajem mieszkania');
insert into category values(DEFAULT, 'INCOME', 'Allegro sprzedaż');
insert into category values(DEFAULT, 'INCOME', 'Fundusz inwestycyjny oprocentowanie');
insert into category values(DEFAULT, 'INCOME', 'Umowa zlecenie');
insert into category values(DEFAULT, 'EXPENSE', 'Oplaty za media');
insert into category values(DEFAULT, 'EXPENSE', 'Oplaty samochod');
insert into category values(DEFAULT, 'EXPENSE', 'Oplaty za czesne');
insert into category values(DEFAULT, 'EXPENSE', 'Paliwo');
insert into category values(DEFAULT, 'EXPENSE', 'Artykuły sporzywcze');
insert into category values(DEFAULT, 'EXPENSE', 'Abonament na stołówke');
insert into category values(DEFAULT, 'EXPENSE', 'Piwo');


insert into user values(DEFAULT, 'jan.kowalski@gmail.com', '$2a$10$V03Dn2pMCYQsJ.0I8Wb94ewBqhyD0SLSlT4niKRBv9R84rneTVsdC', 'USER');
insert into wallet values(DEFAULT, (select id from user where login = 'jan.kowalski@gmail.com'), 'Konto bankowe');
insert into wallet values(DEFAULT, (select id from user where login = 'jan.kowalski@gmail.com'), 'Gotówka');
insert into wallet_user values(3,(select id from user where login = 'jan.kowalski@gmail.com'));
insert into wallet_user values(4,(select id from user where login = 'jan.kowalski@gmail.com'));
insert into financialgoal values(DEFAULT, 'samochod', 3);
insert INTO pendingmoneyflow VALUES (DEFAULT, 3400, '2015-09-10 19:17:10', 'Wodociągi Gliwice', '0 0 10 * * ?', (select id from category where description = 'Wypłata'), null, (select id from user where login = 'jan.kowalski@gmail.com'), 3);
insert INTO pendingmoneyflow VALUES (DEFAULT, 700, '2015-09-11 19:15:10', 'wynajem mieszkania', '0 0 10 * * ?', (select id from category where description = 'Wynajem mieszkania'), null, (select id from user where login = 'jan.kowalski@gmail.com'), 4);
insert INTO pendingmoneyflow VALUES (DEFAULT, -150, '2015-09-12 19:15:10', 'oplata za media', '0 0 */10 * * ?', (select id from category where description = 'Oplaty za media'), null, (select id from user where login = 'jan.kowalski@gmail.com'), 3);



insert into user values(DEFAULT, 'dawid.nowak@gmail.com', '$2a$10$6oKRTP2kdCB3K92mHR8pC.tawAU4ux8aahxBLz3D0MVU3I0PNSgwy', 'USER');
insert into wallet values(DEFAULT, (select id from user where login = 'dawid.nowak@gmail.com'), 'Konto bankowe');
insert into wallet values(DEFAULT, (select id from user where login = 'dawid.nowak@gmail.com'), 'Karta kredytowa');
insert into wallet_user values(5,(select id from user where login = 'dawid.nowak@gmail.com'));
insert into wallet_user values(6,(select id from user where login = 'dawid.nowak@gmail.com'));
insert into financialgoal values(DEFAULT, 'nowe okna', 6);
insert INTO pendingmoneyflow VALUES (DEFAULT, 6400, '2015-09-01 19:17:10', 'Blacharnia', '0 0 10 * * ?', (select id from category where description = 'Wypłata'), null, (select id from user where login = 'dawid.nowak@gmail.com'), 5);
insert INTO pendingmoneyflow VALUES (DEFAULT, 800, '2015-09-02 19:15:10', 'Wynajem mieszkania', '0 0 10 * * ?', (select id from category where description = 'Wynajem mieszkania'), null, (select id from user where login = 'dawid.nowak@gmail.com'), 5);
insert INTO pendingmoneyflow VALUES (DEFAULT, -1900, '2015-09-03 19:15:10', 'oplata za czesne', '0 0 */10 * * ?', (select id from category where description = 'Oplaty za czesne'), null, (select id from user where login = 'dawid.nowak@gmail.com'), 5);



insert into user values(DEFAULT, 'adrian.polak@onet.pl', '$2a$10$CpMEHQPJtHPzvGqxMYvKMuak7.QuIPOeF4iUMQSNs7eTRA/MufROW', 'USER');
insert into wallet values(DEFAULT, (select id from user where login = 'adrian.polak@onet.pl'), 'Konto bankowe');
insert into wallet values(DEFAULT, (select id from user where login = 'adrian.polak@onet.pl'), 'Karta kredytowa');
insert into wallet_user values(7,(select id from user where login = 'adrian.polak@onet.pl'));
insert into wallet_user values(8,(select id from user where login = 'adrian.polak@onet.pl'));
insert into financialgoal values(DEFAULT, 'skuter', 8);
insert INTO pendingmoneyflow VALUES (DEFAULT, 3400, '2015-09-11 19:17:10', 'Polska Nafta', '0 0 10 * * ?', (select id from category where description = 'Wypłata'), null, (select id from user where login = 'adrian.polak@onet.pl'), 7);
insert INTO pendingmoneyflow VALUES (DEFAULT, 1200, '2015-09-11 19:15:10', 'Umowa zlecenie', '0 0 10 * * ?', (select id from category where description = 'Umowa zlecenie'), null, (select id from user where login = 'adrian.polak@onet.pl'), 8);
insert INTO pendingmoneyflow VALUES (DEFAULT, -900, '2015-09-11 19:15:10', 'Paliwo', '0 0 */10 * * ?', (select id from category where description = 'Paliwo'), null, (select id from user where login = 'adrian.polak@onet.pl'), 8);




insert into user values(DEFAULT, 'alicja.basik@interia.pl', '$2a$10$wnmPsF0bZJNiWxGZ5PdXCe1kyyfqRRoxxabXwrCUIp6Bv7q7mSLTG', 'USER');
insert into wallet values(DEFAULT, (select id from user where login = 'alicja.basik@interia.pl'), 'Konto bankowe');
insert into wallet values(DEFAULT, (select id from user where login = 'alicja.basik@interia.pl'), 'Gotówka');
insert into wallet_user values(9,(select id from user where login = 'alicja.basik@interia.pl'));
insert into wallet_user values(10,(select id from user where login = 'alicja.basik@interia.pl'));
insert into financialgoal values(DEFAULT, 'nowe mieszkanie', 9);
insert INTO pendingmoneyflow VALUES (DEFAULT, 2400, '2015-09-10 19:17:11', 'Przedszkole', '0 0 10 * * ?', (select id from category where description = 'Wypłata'), null, (select id from user where login = 'alicja.basik@interia.pl'), 9);
insert INTO pendingmoneyflow VALUES (DEFAULT, 500, '2015-09-11 19:15:08', 'Allegro sprzedaż', '0 0 10 * * ?', (select id from category where description = 'Allegro sprzedaż'), null, (select id from user where login = 'alicja.basik@interia.pl'), 10);
insert INTO pendingmoneyflow VALUES (DEFAULT, -200, '2015-09-12 19:15:03', 'Abonament na stołówke', '0 0 */10 * * ?', (select id from category where description = 'Abonament na stołówke'), null, (select id from user where login = 'alicja.basik@interia.pl'), 10);


insert into user values(DEFAULT, 'dawid.kruszynski@gmail.com', '$2a$10$/vmgz05B90L5igUaLFHTRu9EvbWwtgCYb6RD94NMRL/wBTbkCdmUW', 'USER');
insert into wallet values(DEFAULT, (select id from user where login = 'dawid.kruszynski@gmail.com'), 'Konto bankowe');
insert into wallet values(DEFAULT, (select id from user where login = 'dawid.kruszynski@gmail.com'), 'Gotówka');
insert into wallet_user values(11,(select id from user where login = 'dawid.kruszynski@gmail.com'));
insert into wallet_user values(12,(select id from user where login = 'dawid.kruszynski@gmail.com'));
insert into financialgoal values(DEFAULT, 'rower', 11);
insert INTO pendingmoneyflow VALUES (DEFAULT, 6400, '2015-09-14 19:17:10', 'Salon Ferrari', '0 0 10 * * ?', (select id from category where description = 'Wypłata'), null, (select id from user where login = 'dawid.kruszynski@gmail.com'), 11);
insert INTO pendingmoneyflow VALUES (DEFAULT, 500, '2015-09-14 19:15:10', 'Fundusz inwestycyjny oprocentowanie', '0 0 10 * * ?', (select id from category where description = 'Fundusz inwestycyjny oprocentowanie'), null, (select id from user where login = 'dawid.kruszynski@gmail.com'), 11);
insert INTO pendingmoneyflow VALUES (DEFAULT, -200, '2015-09-14 19:15:10', 'Piwo', '0 0 */10 * * ?', (select id from category where description = 'Piwo'), null, (select id from user where login = 'dawid.kruszynski@gmail.com'), 11);


insert into moneyflow values(DEFAULT, 900, '2015-09-14 19:17:10','Rata',((select id from category where description = 'Opłaty samochod')),(select id from financialgoal where name = 'samochod'),2,3);
insert into moneyflow values(DEFAULT, 2000, '2015-09-14 19:17:10','Rata',((select id from category where description = 'Opłaty samochod')),(select id from financialgoal where name = 'samochod'),3,5);
insert into moneyflow values(DEFAULT, 100, '2015-09-14 19:17:10','Rata',((select id from category where description = 'nowe okna')),(select id from financialgoal where name = 'nowe okna'),4,7);
insert into moneyflow values(DEFAULT, 100, '2015-09-14 19:17:10','Rata',((select id from category where description = 'nowe okna')),(select id from financialgoal where name = 'nowe okna'),5,9);
insert into moneyflow values(DEFAULT, 300, '2015-09-14 19:17:10','Rata',((select id from category where description = 'nowe mieszkanie')),(select id from financialgoal where name = 'nowe mieszkanie'),6,12);

commit;