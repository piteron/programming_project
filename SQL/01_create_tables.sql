CREATE
    TABLE
        USER( id BIGINT NOT NULL auto_increment, login VARCHAR( 255 ), PASSWORD VARCHAR( 255 ), type VARCHAR( 255 ), PRIMARY KEY( id ) ) ENGINE = InnoDB;

CREATE
    TABLE
        category(
            id BIGINT NOT NULL auto_increment,
            categoryType VARCHAR(255),
            description VARCHAR(255),
            PRIMARY KEY(id)
        ) ENGINE = InnoDB;

CREATE
    TABLE
        Wallet(
            id BIGINT NOT NULL auto_increment,
            owningUser_id BIGINT,
            name VARCHAR(255),
            PRIMARY KEY(id),
            KEY `FK_WALLET_USER`(`owningUser_id`),
            CONSTRAINT `FK_WALLET_USER` FOREIGN KEY(`owningUser_id`) REFERENCES `user`(`id`)
        ) ENGINE = InnoDB;

CREATE
    TABLE
        financialgoal(
            id BIGINT NOT NULL auto_increment,
            name VARCHAR(255),
            wallet_id BIGINT,
            PRIMARY KEY(id),
            KEY `FK_FINANCIALGOAL_WALLET`(`wallet_id`),
            CONSTRAINT `FK_FINANCIALGOAL_WALLET` FOREIGN KEY(`wallet_id`) REFERENCES `wallet`(`id`)
        ) ENGINE = InnoDB;

CREATE
    TABLE
        moneyflow(
            id BIGINT NOT NULL auto_increment,
            amount DECIMAL(
                19,
                2
            ),
            DATE DATETIME,
            description VARCHAR(255),
            category_id BIGINT,
            financialGoal_id BIGINT,
            user_id BIGINT,
            wallet_id BIGINT,
            PRIMARY KEY(id),
            KEY `FK_MONEYFLOW_CATEGORY`(`category_id`),
            KEY `FK_MONEYFLOW_FINANCIALGOAL`(`financialGoal_id`),
            KEY `FK_MONEYFLOW_USER`(`user_id`),
            KEY `FK_MONEYFLOW_WALLET`(`wallet_id`),
            CONSTRAINT `FK_MONEYFLOW_USER` FOREIGN KEY(`user_id`) REFERENCES `user`(`id`),
            CONSTRAINT `FK_MONEYFLOW_FINANCIALGOAL` FOREIGN KEY(`financialGoal_id`) REFERENCES `financialgoal`(`id`),
            CONSTRAINT `FK_MONEYFLOW_CATEGORY` FOREIGN KEY(`category_id`) REFERENCES `category`(`id`),
            CONSTRAINT `FK_MONEYFLOW_WALLET` FOREIGN KEY(`wallet_id`) REFERENCES `wallet`(`id`)
        ) ENGINE = InnoDB;

CREATE
    TABLE
        PendingMoneyFlow(
            id BIGINT NOT NULL auto_increment,
            amount DECIMAL(
                19,
                2
            ),
            DATE DATETIME,
            description VARCHAR(255),
            cronExpression VARCHAR(255),
            category_id BIGINT,
            financialGoal_id BIGINT,
            user_id BIGINT,
            wallet_id BIGINT,
            PRIMARY KEY(id),
            KEY `FK_PENDINGMONEYFLOW_CATEGORY`(`category_id`),
            KEY `FK_PENDINGMONEYFLOW_FINANCIALGOAL`(`financialGoal_id`),
            KEY `FK_PENDINGMONEYFLOW_USER`(`user_id`),
            KEY `FK_PENDINGMONEYFLOW_WALLET`(`wallet_id`),
            CONSTRAINT `FK_PENDINGMONEYFLOW_USER` FOREIGN KEY(`user_id`) REFERENCES `user`(`id`),
            CONSTRAINT `FK_PENDINGMONEYFLOW_FINANCIALGOAL` FOREIGN KEY(`financialGoal_id`) REFERENCES `financialgoal`(`id`),
            CONSTRAINT `FK_PENDINGMONEYFLOW_CATEGORY` FOREIGN KEY(`category_id`) REFERENCES `category`(`id`),
            CONSTRAINT `FK_PENDINGMONEYFLOW_WALLET` FOREIGN KEY(`wallet_id`) REFERENCES `wallet`(`id`)
        ) ENGINE = InnoDB;

CREATE
    TABLE
        Wallet_User(
            wallet_id BIGINT NOT NULL,
            user_id BIGINT NOT NULL,
            PRIMARY KEY(
                wallet_id,
                user_id
            ),
            CONSTRAINT FK_WALLET_USER_WALLET FOREIGN KEY(wallet_id) REFERENCES Wallet(id),
            CONSTRAINT FK_WALLET_USER_USER FOREIGN KEY(user_id) REFERENCES USER( id )
        ) ENGINE = InnoDB;