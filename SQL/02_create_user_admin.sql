insert into user values(DEFAULT, 'admin', '$2a$10$Xmud6hqK1DMLdw5T2NqW/.ZXoNzz4iGbO1KlDB9hA3c1Pcrj7OnLu', 'ADMIN');
insert into category values(DEFAULT, 'INCOME', 'Wypłata');
insert into category values(DEFAULT, 'INCOME', 'Kieszonkowe');
insert into category values(DEFAULT, 'EXPENSE', 'Abonament telefoniczny');
insert into wallet values(DEFAULT, (select id from user where login = 'admin'), 'Pierwszy portfel');
insert into wallet values(DEFAULT, (select id from user where login = 'admin'), 'Drugi Portfel');
insert into financialgoal values(DEFAULT, 'siekiera', (select id from wallet where name = 'Pierwszy portfel'));
insert INTO pendingmoneyflow VALUES (DEFAULT, 2200, '2015-08-19 19:17:10', 'Perform Poland', '0 0 */6 * * ?', (select id from category where description = 'Wypłata'), null, (select id from user where login = 'admin'), (select id from wallet where name = 'Pierwszy portfel'));
insert INTO pendingmoneyflow VALUES (DEFAULT, 100, '2015-08-19 19:15:10', 'kieszonkowe', '0 0 */3 * * ?', (select id from category where description = 'Kieszonkowe'), null, (select id from user where login = 'admin'), (select id from wallet where name = 'Drugi portfel'));
insert INTO pendingmoneyflow VALUES (DEFAULT, -30, '2015-08-19 19:15:10', 'danina', '0 0 */1 * * ?', (select id from category where description = 'Abonament telefoniczny'), null, (select id from user where login = 'admin'), (select id from wallet where name = 'Pierwszy portfel'));
commit;